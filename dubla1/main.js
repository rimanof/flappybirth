var bg = document.createElement("IMG");
bg.setAttribute("src", "image/bg.png");
bg.style.width=100+"%";
bg.style.height=100+"%";
bg.style.position='relative';
document.body.appendChild(bg);

//variabile
var pipedistance=bg.offsetHeight/5;
var birdX=parseInt(bg.width/10);
var birdY=parseInt(bg.height/3);
var gravity=parseInt(1);

var pipeUp = document.createElement("IMG");
pipeUp.setAttribute("src", "image/pipeUp.png");
pipeUp.style.width=7+"%";
pipeUp.style.height=30+"%";
pipeUp.style.position='absolute';
document.body.appendChild(pipeUp);
pipeUp.style.left=bg.offsetWidth/2+pipeUp.offsetWidth+"px";
pipeUp.style.top=0;

var pipeBottom = document.createElement("IMG");
pipeBottom.setAttribute("src", "image/pipeBottom.png");
pipeBottom.style.width=7+"%";
pipeBottom.style.height=bg.offsetHeight-pipedistance-pipeUp.offsetHeight.toFixed(0)-2+"px";
pipeBottom.style.position='absolute';
document.body.appendChild(pipeBottom);
pipeBottom.style.left=bg.offsetWidth/2+pipeBottom.offsetWidth+"px";
pipeBottom.style.top=bg.offsetHeight-pipedistance-pipeUp.offsetHeight+"px";

var fg = document.createElement("IMG");
fg.setAttribute("src", "image/fg.png");
fg.style.width=100+"%";
fg.style.height=20+"%";
fg.style.position='absolute';
document.body.appendChild(fg);
fg.style.top=bg.offsetHeight-fg.offsetHeight+"px";

var bird = document.createElement("IMG");
bird.setAttribute("src", "image/bird.png");
bird.style.width=4+"%";
bird.style.height=4+"%";
bird.style.position='absolute';
document.body.appendChild(bird);
bird.style.left=birdX+"px";
bird.style.top=birdY+"px";

bird.onload(change());
function change(){
    bird.style.top=birdY+gravity+"px";
    gravity++;
    setInterval(change,3000);
}